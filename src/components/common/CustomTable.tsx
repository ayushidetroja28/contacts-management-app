import PropTypes from "prop-types";

const CustomTable = (props: any) => {
  return (
    <div>
      <div className="bg-white inline-block min-w-full shadow-md rounded-lg overflow-hidden">
        <table className="min-w-full leading-normal overflow-visible">
          {/* column headers */}
          <thead>
            <tr>{props.columnHeaders}</tr>
          </thead>

          {/* table body */}
          <tbody>{props.dataRows}</tbody>
        </table>
        {/* no data available */}
        {props?.results?.length === 0 && (
          <div className="m-4 flex justify-center">
            <p>No Data Found</p>
          </div>
        )}
      </div>
    </div>
  );
};

// component props validation
CustomTable.propTypes = {
  columnHeaders: PropTypes.object,
  results: PropTypes.any,
  dataRows: PropTypes.object,
  isSearch: PropTypes.bool,
  getSearchValue: PropTypes.func,
  handlePageChange: PropTypes.func,
  style: PropTypes.any,
};

export default CustomTable;
