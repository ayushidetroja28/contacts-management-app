import "./loader.css";

const CustomSpinner = () => {
  return (
    <div className="LoadingContainer">
      <div className="LoadingBox"></div>
    </div>
  );
};
export default CustomSpinner;
