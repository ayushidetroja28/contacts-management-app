import { useState } from "react";

import { faEdit, faEye, faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import CustomTable from "src/components/common/CustomTable";
import { table_header_css, table_row_css } from "src/utils/constants";
import { useAppSelector } from "../redux/hooks";
import AddContact from "./AddContact";
import DeleteContact from "./DeleteContact";

const Contact = () => {
  const [modal, setModal] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [id, setId] = useState(0);
  const [type, setType] = useState("add");

  const [array, setArray] = useState(
    useAppSelector((state) => state.contact.value)
  );

  // column header for contacts
  const ColumnHeaders = () => {
    return (
      <>
        <th className={table_header_css}>No</th>
        <th className={table_header_css}>First Name</th>
        <th className={table_header_css}>Last Name</th>
        <th className={table_header_css}>Status</th>
        <th className={table_header_css}>Action</th>
      </>
    );
  };
  // column row for contacts data
  const DataRows = () => {
    return (
      <>
        {(array || []).map((item, index) => (
          <tr className="text-center" key={index}>
            <td className={table_row_css}>{index + 1}</td>
            <td className={table_row_css}>{item?.firstName}</td>
            <td className={table_row_css}>{item?.lastName}</td>
            <td className={table_row_css}>{item?.status}</td>
            <td className={table_row_css}>
              <div className="flex cursor-pointer">
                <div
                  onClick={() => {
                    setId(index);
                    setType("edit");
                    setModal(true);
                  }}
                >
                  <FontAwesomeIcon
                    icon={faEdit}
                    className="text-sm ml-4 text-blue-500"
                  />
                </div>
                <div
                  onClick={() => {
                    setId(index);
                    setDeleteModal(true);
                  }}
                >
                  <FontAwesomeIcon
                    icon={faTrash}
                    className="text-sm ml-4 text-red-500"
                  />
                </div>
                <div
                  color="flat-primary"
                  onClick={() => {
                    setId(index);
                    setType("view");
                    setModal(true);
                  }}
                >
                  <FontAwesomeIcon
                    icon={faEye}
                    className="text-sm ml-4 text-green-500"
                  />
                </div>
              </div>
            </td>
          </tr>
        ))}
      </>
    );
  };

  return (
    <div>
      {/* modal for add, edit and view contact */}
      {modal && (
        <AddContact
          setModal={setModal}
          id={id}
          setArray={setArray}
          array={array}
          type={type}
        />
      )}

      {/* modal for delete contact */}
      {deleteModal && (
        <DeleteContact
          setModal={setDeleteModal}
          id={id}
          setArray={setArray}
          array={array}
        />
      )}
      <div className="flex justify-between mt-3">
        <span className="text-2xl font-semibold text-blue-500">Contacts</span>
        {/* add contact button */}
        <button
          onClick={() => {
            setType("add");
            setModal(true);
          }}
          className="px-2 py-1 text-white bg-blue-500 samibold rounded white-space-nowrap w-100"
        >
          Add Contact
        </button>
      </div>

      <div className="mt-5">
        {/* table for display contact data */}
        <CustomTable
          columnHeaders={<ColumnHeaders />}
          dataRows={<DataRows />}
          results={array}
        />
      </div>
    </div>
  );
};

export default Contact;
