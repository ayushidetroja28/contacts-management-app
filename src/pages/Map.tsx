import { Icon } from "leaflet";
import "leaflet/dist/leaflet.css";
import { useEffect, useState } from "react";
import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";
import { useQuery } from "react-query";
import { axiosApi } from "src/helpers/axios";
import WorldCards from "./WorldCards";

const customIcon = new Icon({
  iconUrl: "https://unpkg.com/leaflet@1.9.3/dist/images/marker-icon.png",
  iconSize: [20, 30],
  // iconAnchor: [1, 1],
  // popupAnchor: [-0, -76]
});

export const Maps = () => {
  const [maps, setMap] = useState<any>([]);

  const getAllData = async () => {
    const res = await axiosApi.get<any>(`/countries`);
    return res.data;
  };
  // Using the hook
  const { data, error, isLoading } = useQuery("covidCountry", getAllData);

  useEffect(() => {
    if (data !== undefined) {
      setMap(data);
    }
  }, [data]);

  return (
    <div>
      {/* world wide data */}
      <WorldCards />
      <div className="mt-3">
        {/* map container */}
        <MapContainer center={[20, 77]} zoom={5} scrollWheelZoom={true}>
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          {/* country marker with popup */}
          {(maps || [])?.map((item: any, index: number) => (
            <Marker
              key={index}
              position={[item?.countryInfo?.lat, item?.countryInfo?.long]}
              icon={customIcon}
            >
              <Popup>
                Country : {`${item?.country}`} <br /> Active cases :
                {`${item?.cases}`}
                <br />
                Recovered cases : {`${item?.recovered}`}
                <br />
                Deaths : {`${item?.deaths}`}
              </Popup>
            </Marker>
          ))}
        </MapContainer>
      </div>
    </div>
  );
};
