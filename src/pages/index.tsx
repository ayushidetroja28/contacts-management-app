import React from "react";
import { Route, Routes } from "react-router-dom";
import Contact from "./Contact";
import Charts from "./Charts";

const Dashboard = () => {
  return (
    <>
      {/* routes */}
      <Routes>
        <Route path="/" element={<Contact />} />
        <Route path="/charts" element={<Charts />} />
      </Routes>
    </>
  );
};

export default Dashboard;
