import React from "react";
import { CloseIcon } from "src/assets/img";
import { increment } from "src/redux/contact/slice";
import { useAppDispatch } from "src/redux/hooks";

const DeleteContact = ({
  setModal,
  id,
  setArray,
  array,
}: {
  setModal: React.Dispatch<React.SetStateAction<boolean>>;
  id: number;
  setArray: any;
  array: any;
}) => {
  const dispatch = useAppDispatch();

  const clickOnSubmit = () => {
    let a = [...array];
    a.splice(id, 1);
    setArray(a);
    dispatch(increment(array));
    setModal(false);
  };

  return (
    <div className="fixed inset-0 z-[52] overflow-y-auto">
      {/* delete contact modal */}
      <div className="fixed inset-0 w-full h-full backdrop-blur-sm bg-black/50" />
      <div className="flex items-center min-h-screen px-4 py-8 cursor-pointer">
        <div className="relative w-full max-w-lg sm:mx-auto mx-2">
          <div className="bg-white shadow-sm rounded-lg">
            <div className="flex justify-between p-5">
              <h1 className="text-themecolor text-start sm:text-xl text-md">{`Delete Contact`}</h1>
              <div
                className="w-6 h-6 bg-white/30 rounded my-auto"
                onClick={() => setModal(false)}
              >
                <img src={CloseIcon} alt="" className="w-6 h-6 p-1" />
              </div>
            </div>
            <div className="flex justify-center px-5">
              <div className="w-full text-gray-700">
                <div className="mb-5">
                  <span>{`Are you sure you want to delete this contact?`}</span>
                  <div className="form-group">
                    <div className="flex justify-end mt-2">
                      <div className="flex justify-end">
                        <button
                          className={`bg-transparent border border-1 border-slate-500 flex px-4 py-2 rounded-lg font-light text-slate-500 text-sm my-3`}
                          onClick={() => setModal(false)}
                        >
                          CANCEL
                        </button>
                        <button
                          onClick={clickOnSubmit}
                          className={`bg-blue-500 ml-3 flex px-4 py-2 rounded-lg font-light text-white text-sm my-3`}
                        >
                          DELETE
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DeleteContact;
