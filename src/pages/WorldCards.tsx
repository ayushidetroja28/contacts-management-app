import { useQuery } from "react-query";
import { axiosApi } from "src/helpers/axios";

const WorldCards = () => {
  const getAllData = async () => {
    const res = await axiosApi.get<any>(`/all`);
    return res.data;
  };
  // Using the hook
  const { data, error, isLoading } = useQuery("covidWorld", getAllData);

  return (
    <div className="grid grid-cols-3 gap-4">
      {/* total data of world covid cases */}
      <div className="p-5 bg-slate-300 rounded-md shadow-sm text-center">
        <div>
          <p className="font-bold pb-0">Total cases</p>
          {data?.cases || 0}
        </div>
      </div>
      <div className="p-5 bg-slate-300 rounded-md shadow-sm text-center">
        <div>
          <p className="font-bold pb-0">Total recovered</p>
          {data?.recovered || 0}
        </div>
      </div>
      <div className="p-5 bg-slate-300 rounded-md shadow-sm text-center">
        <div>
          <p className="font-bold pb-0">Total deaths</p>
          <p className="pb-0">{data?.deaths || 0}</p>
        </div>
      </div>
    </div>
  );
};

export default WorldCards;
