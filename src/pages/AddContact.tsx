import { ErrorMessage, Field, Form, Formik } from "formik";
import React, { useEffect, useState } from "react";
import { CloseIcon } from "src/assets/img";
import { increment } from "src/redux/contact/slice";
import { useAppDispatch, useAppSelector } from "src/redux/hooks";
import * as Yup from "yup";

const AddContact = ({
  setModal,
  id,
  setArray,
  array,
  type,
}: {
  setModal: React.Dispatch<React.SetStateAction<boolean>>;
  id: number;
  setArray: any;
  array: any;
  type: string;
}) => {
  const dispatch = useAppDispatch();

  const radioHandler = (
    event: React.ChangeEvent<HTMLInputElement>,
    setFieldValue: any
  ) => {
    setFieldValue("status", event.target.value);
  };

  const [disable, setDisable] = useState(false);

  useEffect(() => {
    type === "view" && setDisable(true);
  }, [type]);

  return (
    <div className="fixed inset-0 z-[52] overflow-y-auto">
      {/* modal for add, edit and view contact */}
      <div className="fixed inset-0 w-full h-full backdrop-blur-sm bg-black/50" />
      <div className="flex items-center min-h-screen px-4 py-8 cursor-pointer">
        <div className="relative w-full max-w-lg sm:mx-auto mx-2">
          <div className="bg-white shadow-sm rounded-lg">
            <div className="flex justify-between p-5">
              <h1 className="text-themecolor text-start sm:text-xl text-md">{`${
                type === `add` ? `Add` : type === `edit` ? `Edit` : `View`
              } Contact`}</h1>

              {/* close icon to modal close */}
              <div
                className="w-6 h-6 bg-white/30 rounded my-auto"
                onClick={() => setModal(false)}
              >
                <img src={CloseIcon} alt="" className="w-6 h-6 p-1" />
              </div>
            </div>
            <div className="flex justify-center px-5">
              <div className="w-full text-gray-700">
                <div className="mb-5">
                  {/* formik start */}
                  <Formik
                    initialValues={{
                      firstName: type === "add" ? "" : array[id]?.firstName,
                      lastName: type === "add" ? "" : array[id]?.lastName,
                      status: type === "add" ? "" : array[id]?.status,
                    }}
                    // validation
                    validationSchema={Yup.object().shape({
                      status: Yup.string().required("Status is required"),
                      lastName: Yup.string().required("Last name is required"),
                      firstName: Yup.string().required(
                        "First Name is required"
                      ),
                    })}
                    // onclick submit button
                    onSubmit={(fields) => {
                      let a = [...array];
                      type === "add" ? a.push(fields) : (a[id] = fields);
                      setArray(a);
                      dispatch(increment(array));
                      setModal(false);
                    }}
                    render={({ values, setFieldValue }) => (
                      <Form className="w-full h-full">
                        {/* {loading && <CustomSpinner />} */}
                        <div className="mx-auto ">
                          <div className="mt-2 flex flex-col">
                            <label
                              className="text-gray-500 py-2 pt-3 text-xs"
                              htmlFor="firstName"
                            >
                              First Name
                            </label>
                            <Field
                              disabled={disable}
                              className="block w-full outline-none bg-transparent border-0 border-b border-bordergrey/75 pb-2 mb-1"
                              type={"text"}
                              name="firstName"
                              id="firstName"
                              placeholder="Enter First Name"
                            />

                            <ErrorMessage
                              name="firstName"
                              component="div"
                              className="text-red-500 text-xs"
                            />
                          </div>
                          <div className="mt-2 flex flex-col">
                            <label
                              className="text-gray-500 py-2 pt-3 text-xs"
                              htmlFor="lastName"
                            >
                              Last Name
                            </label>
                            <Field
                              disabled={disable}
                              className="block w-full outline-none bg-transparent border-0 border-b border-bordergrey/75 pb-2 mb-1"
                              type="text"
                              name="lastName"
                              id="lastName"
                              placeholder="Enter Last name"
                            />

                            <ErrorMessage
                              name="lastName"
                              component="div"
                              className="text-red-500 text-xs"
                            />
                          </div>
                          <div className="mt-2 flex flex-col">
                            <label
                              className="text-gray-500 py-2 pt-3 text-xs"
                              htmlFor="status"
                            >
                              Status
                            </label>
                            <Field
                              disabled={disable}
                              name="status"
                              render={({ field }: { field: any }) => (
                                <>
                                  <div className="radio-item">
                                    <input
                                      {...field}
                                      disabled={disable}
                                      id="active"
                                      value="Active"
                                      checked={values.status === "Active"}
                                      name="type"
                                      type="radio"
                                      onChange={(e) =>
                                        radioHandler(e, setFieldValue)
                                      }
                                    />
                                    <label className="ms-2" htmlFor="active">
                                      Active
                                    </label>
                                  </div>

                                  <div className="radio-item">
                                    <input
                                      {...field}
                                      disabled={disable}
                                      id="inActive"
                                      value="Inactive"
                                      name="type"
                                      checked={values.status === "Inactive"}
                                      type="radio"
                                      onChange={(e) =>
                                        radioHandler(e, setFieldValue)
                                      }
                                    />
                                    <label className="ms-2" htmlFor="inActive">
                                      InActive
                                    </label>
                                  </div>
                                </>
                              )}
                            />

                            <ErrorMessage
                              name="status"
                              component="div"
                              className="text-red-500 text-xs"
                            />
                          </div>
                        </div>

                        <div className="form-group">
                          <div className="flex justify-end mt-2">
                            <div className="flex justify-end">
                              <button
                                className={`bg-transparent border border-1 border-slate-500 flex px-4 py-2 rounded-lg font-light text-slate-500 text-sm my-3`}
                                onClick={() => setModal(false)}
                              >
                                CANCEL
                              </button>
                              <button
                                type="submit"
                                disabled={disable}
                                className={`bg-blue-500 ml-3 flex px-4 py-2 rounded-lg font-light text-white text-sm my-3`}
                              >
                                SUBMIT
                              </button>
                            </div>
                          </div>
                        </div>
                      </Form>
                    )}
                  />
                  {/* formik end */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddContact;
