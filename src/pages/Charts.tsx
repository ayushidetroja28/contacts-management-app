import { Chart as ChartJS, registerables } from "chart.js";
import { useEffect, useState } from "react";
import { Line } from "react-chartjs-2";
import { useQuery } from "react-query";
import CustomSpinner from "src/components/customSpinner";
import { axiosApi } from "src/helpers/axios";
import { Maps } from "./Map";
ChartJS.register(...registerables);

const Charts = () => {
  const [year, setYear] = useState<number>(2020);
  const [month, setMonth] = useState<number>(1);
  const [date, setDate] = useState<any>([]);
  const [array, setArray] = useState<any>([]);
  const [caseArray, setCaseArray] = useState<any>([]);
  const [deathArray, setDeathArray] = useState<any>([]);
  const [recoverArray, setRecoverArray] = useState<any>([]);

  const getCharts = async () => {
    const res = await axiosApi.get<any>(`/historical/all?lastdays=all`);
    return res.data;
  };
  // Using the hook
  const { data, error, isLoading } = useQuery("ChartsData", getCharts);

  const [cases, setCase] = useState<any>({});
  const [deaths, setDeath] = useState<any>({});
  const [recover, setRecover] = useState<any>({});

  useEffect(() => {
    if (data !== undefined) {
      setCase(data?.cases);
      setDeath(data?.deaths);
      setRecover(data?.recovered);
    }
  }, [data]);

  useEffect(() => {
    if (
      month === 1 ||
      month === 3 ||
      month === 5 ||
      month === 7 ||
      month === 8 ||
      month === 10 ||
      month === 12
    ) {
      setDate([
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
        21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
      ]);
      setArray([
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0,
      ]);
    } else if (month === 4 || month === 6 || month === 9 || month === 11) {
      setDate([
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
        21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
      ]);
      setArray([
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
      ]);
    } else {
      if (year % 4 == 0 && year % 100 != 0 && month === 2) {
        setDate([
          1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
          21, 22, 23, 24, 25, 26, 27, 28, 29,
        ]);
        setArray([
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0, 0, 0, 0,
        ]);
      } else {
        setDate([
          1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
          21, 22, 23, 24, 25, 26, 27, 28,
        ]);
        setArray([
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
          0, 0, 0, 0, 0,
        ]);
      }
    }
  }, [year, month]);

  useEffect(() => {
    if (data) {
      let a = [...array];
      for (let key in cases) {
        if (
          new Date(key).getFullYear() === year &&
          new Date(key).getMonth() === month - 1
        ) {
          a[new Date(key).getDate() - 1] = cases[key as keyof Object] || 0;
        }
      }
      setCaseArray(a);
    }
  }, [cases, array]);

  useEffect(() => {
    if (data !== undefined) {
      let a = [...array];
      for (let key in deaths) {
        if (
          new Date(key).getFullYear() === year &&
          new Date(key).getMonth() === month - 1
        ) {
          a[new Date(key).getDate() - 1] = deaths[key as keyof Object] || 0;
        }
      }
      setDeathArray(a);
    }
  }, [deaths, array]);

  useEffect(() => {
    if (data !== undefined) {
      let a = [...array];
      for (let key in recover) {
        if (
          new Date(key).getFullYear() === year &&
          new Date(key).getMonth() === month - 1
        ) {
          a[new Date(key).getDate() - 1] = recover[key as keyof Object] || 0;
        }
      }
      setRecoverArray(a);
    }
  }, [recover, array]);

  const [chartData, setChartData] = useState<any>({
    labels: date,
    datasets: [
      {
        label: "Cases",
        data: caseArray,
        fill: true,
        backgroundColor: "#f7cccc6b",
        borderColor: "#ff0000",
      },
      {
        label: "Death",
        data: deathArray,
        fill: false,
        borderColor: "#000000",
      },
      {
        label: "Receover",
        data: recoverArray,
        fill: false,
        borderColor: "rgba(0, 217, 84,1)",
      },
    ],
  });

  useEffect(() => {
    if (
      caseArray?.length > 0 &&
      deathArray?.length > 0 &&
      recoverArray?.length > 0
    ) {
      const chartData = {
        labels: date,
        datasets: [
          {
            label: "Cases",
            data: caseArray,
            fill: true,
            backgroundColor: "#f7cccc6b",
            borderColor: "#ff0000",
          },
          {
            label: "Death",
            data: deathArray,
            fill: false,
            borderColor: "#000000",
          },
          {
            label: "Receover",
            data: recoverArray,
            fill: false,
            borderColor: "rgba(0, 217, 84,1)",
          },
        ],
      };

      setChartData(chartData);
    }
  }, [caseArray, deathArray, recoverArray]);

  return (
    <div>
      {/* loader */}
      {isLoading && <CustomSpinner />}
      <div className="flex justify-between mt-3">
        <span className="text-2xl font-semibold text-blue-500">Charts</span>
      </div>
      <div className="mt-5 bg-white rounded-md shadow-sm p-4">
        {/* change year and month to see different data of covid */}
        <div className="flex justify-end">
          <select
            className="select"
            onChange={(option) => {
              setYear(Number(option?.target?.value));
            }}
          >
            <option value={2020}>2020</option>
            <option value={2021}>2021</option>
            <option value={2022}>2022</option>
            <option value={2023}>2023</option>
          </select>
          <select
            className="select ms-2"
            onChange={(option) => {
              setMonth(Number(option?.target?.value));
            }}
          >
            <option value={1}>Jan</option>
            <option value={2}>Feb</option>
            <option value={3}>Mar</option>
            <option value={4}>Apr</option>
            <option value={5}>May</option>
            <option value={6}>Jun</option>
            <option value={7}>Jul</option>
            <option value={8}>Aug</option>
            <option value={9}>Sep</option>
            <option value={10}>Oct</option>
            <option value={11}>Nov</option>
            <option value={12}>Dec</option>
          </select>
        </div>
        <div className="w-full">
          <span className="flex flex-row items-center ml-2 mb-2 font-bold text-[14px] text-oceanblue">
            Covid cases
          </span>
          {/* chart */}
          {Object.keys(chartData).length !== 0 && <Line data={chartData} />}
        </div>
      </div>

      <div className="flex justify-between mt-5">
        <span className="text-2xl font-semibold text-blue-500">Map</span>
      </div>
      <div className="mt-5 bg-white rounded-md shadow-sm p-4">
        <div className="w-full">
          {/* map for covid */}
          <Maps />
        </div>
      </div>
    </div>
  );
};

export default Charts;
