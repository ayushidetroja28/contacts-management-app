import axios from "axios";
import { ACCESS_TOKEN } from "../utils/constants";

const accessToken = `${localStorage.getItem(ACCESS_TOKEN)}`;

const axiosApi = axios.create({
  baseURL: process.env.REACT_APP_HOST,
  headers: {
    "Content-type": "application/json",
    Authorization: `Bearer ${accessToken}`,
  },
});

axiosApi.interceptors.response.use(
  (response) => response,
  (error) => {
    // const statusCode = error?.response?.status;

    // if (statusCode === 401) {
    //   window.location.href = "/login";
    // }

    return Promise.reject(error);
  }
);

export { axiosApi };
