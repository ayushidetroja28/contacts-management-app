import { createSlice } from "@reduxjs/toolkit";
import type { RootState } from "../store";

// Define a type for the slice state
interface InitState {
  value: Array<any>;
}

// Define the initial state using that type
const initialState: InitState = {
  value: [],
};

export const contactSlice = createSlice({
  name: "contact",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    increment: (state, { payload }) => {
      state.value = payload;
    },
  },
});

export const { increment } = contactSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.contact.value;

export default contactSlice.reducer;
