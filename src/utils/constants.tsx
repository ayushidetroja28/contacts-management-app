// utils keywords
export const ACCESS_TOKEN = "accessToken";
// export const firstCap = ({string}) =>
//   string?.charAt(0)?.toUpperCase() + string?.slice(1);
export const table_header_css =
  "px-5 py-3 font-bold border-b-2 border-gray-200 bg-gray-300 text-left text-xs font-semibold text-gray-700 uppercase tracking-wider";
export const table_row_css =
  "px-5 py-3 text-start border-b border-gray-200 bg-white text-sm";
