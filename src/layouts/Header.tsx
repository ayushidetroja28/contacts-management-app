import React from "react";
import { MenuIcon } from "../assets/img";

const Header = ({
  setDisplay,
  display,
}: {
  setDisplay: React.Dispatch<React.SetStateAction<boolean>>;
  display: boolean;
}) => {
  return (
    <>
      <header className="bg-[#f8f8f8] fixed sm:ml-[210px]  top-0 left-0 right-0  z-40 pt-0">
        <div className="m-2 bg-white text-white rounded-md shadow-sm">
          <nav className="flex flex-wrap pl-4 justify-between items-center">
            <div className="h-[50px] my-auto">
              <div className="h-[50px] my-auto cursor-pointer flex justify-center items-center">
                <div className="text-md my-auto sm:flex hidden">
                  Contact Management App
                </div>
                {/* drawer icon to open */}
                <img
                  onClick={() => setDisplay(!display)}
                  src={MenuIcon}
                  alt=""
                  className="w-4 h-4 flex sm:hidden mr-4 cursor-pointer"
                />
              </div>
            </div>
          </nav>
        </div>
      </header>
    </>
  );
};

export default Header;
