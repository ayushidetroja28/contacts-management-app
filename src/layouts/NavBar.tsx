import React from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { CloseIcon } from "../assets/img";

function NavBar({
  setDisplay,
  display,
}: {
  setDisplay: React.Dispatch<React.SetStateAction<boolean>>;
  display: boolean;
}) {
  const navigate = useNavigate();
  const location = useLocation();
  const changeDisplay = () => {
    setDisplay(!display);
  };

  return display ? (
    <>
      <div
        className="mt-[-50px] h-screen w-full fixed z-50 overflow-auto bg-black/40 "
        onClick={() => changeDisplay()}
      ></div>
      <div className="mt-[-50px] h-screen fixed z-50 overflow-auto w-[200px] bg-white text-blue-500">
        <div className="flex flex-wrap items-center border-b border-solid border-1 border-gray-100/30">
          <div className="h-[50px] w-full flex justify-between">
            <div className="w-full text-sm my-auto font-semibold px-3 text-blue-500">
              Contact Management
            </div>
            {/* drawer icon to close */}
            <img
              alt=""
              src={CloseIcon}
              className="w-3 h-3 my-auto m-2"
              onClick={() => changeDisplay()}
            />
          </div>
        </div>
        <hr />
        <ul className="w-full cursor-pointer px-2 mt-2">
          <li
            onClick={() => {
              navigate("/");
              changeDisplay();
            }}
            className={`px-4 py-3 text-base w-full rounded ${
              location?.pathname === "/" ? `bg-blue-500 text-white` : ``
            }`}
          >
            <div>Contact</div>
          </li>

          <li
            onClick={() => {
              navigate("/charts");
              changeDisplay();
            }}
            className={`px-4 py-3 text-base w-full rounded ${
              location?.pathname?.includes("charts")
                ? `bg-blue-500 text-white`
                : ``
            }`}
          >
            <div>Chasts and Maps</div>
          </li>
        </ul>
      </div>
    </>
  ) : (
    <div className="hidden shadow-sm mt-[-50px] sm:block h-screen fixed z-50 overflow-auto w-[200px] bg-white text-blue-500">
      <div className="text-md my-auto font-semibold py-5 px-3 sm:flex hidden text-blue-500">
        Contact Management
      </div>

      <hr />
      <ul className="w-full cursor-pointer px-2 mt-2">
        <li
          onClick={() => {
            navigate("/");
          }}
          className={`px-4 py-3 text-base w-full rounded ${
            location?.pathname === "/" ? `bg-blue-500 text-white` : ``
          }`}
        >
          <div>Contact</div>
        </li>

        <li
          onClick={() => {
            navigate("/charts");
          }}
          className={`px-4 py-3 text-base w-full rounded ${
            location?.pathname?.includes("charts")
              ? `bg-blue-500 text-white`
              : ``
          }`}
        >
          <div>Chasts and Maps</div>
        </li>
      </ul>
    </div>
  );
}

export default NavBar;
