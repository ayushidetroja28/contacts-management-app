import { useState } from "react";
import { Outlet } from "react-router-dom";
import Header from "./Header";
import NavBar from "./NavBar";

export default function DefaultLayout() {
  const [display, setDisplay] = useState(false);

  return (
    <div>
      {/* header */}
      <Header setDisplay={setDisplay} display={display} />
      {/* nav drawer */}
      <NavBar setDisplay={setDisplay} display={display} />
      <div className="mt-[50px] px-2 py-4 sm:ml-[210px]">
        {/* rander all components here */}
        <Outlet />
      </div>
    </div>
  );
}
