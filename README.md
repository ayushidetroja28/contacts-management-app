# contacts-management-app

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

```
cd existing_repo
git clone origin https://gitlab.com/ayushidetroja28/contacts-management-app.git
git checkout develop
npx install 18.16.0
nvm use 18.16.0
yarn
yarn start  
```
